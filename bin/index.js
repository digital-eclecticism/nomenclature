#!/usr/bin/env node
const CLI = require('../src/CLIClass');
const Nomenclature = require('../src/NomenclatureClass');

new CLI();

module.exports = Nomenclature;