const path = require('path');
const fs = require('fs');
const os = require('os');

module.exports = class Nomenclature {
    constructor({inFile, outFile, separator, affix, concatenate}){
        this.inFile = inFile;
        this.outFile = outFile;
        this.separator = separator;
        this.affix = affix;
        this.concatenate = concatenate;
    }

    generateHash () {
        const hash = require('crypto')
        .createHash('sha256')
        .update(Date.now().toString() + 'PD', 'utf8')
        .digest('hex');
        return hash.length >= 8? hash.substring(0, 7) : hash;
    }

    generateTimestamp(){
        const timestamp = new Date();
        return `${timestamp.getMonth() + 1}${timestamp.getDate()}${timestamp.getFullYear()}${timestamp.getHours()}${timestamp.getMinutes()}${timestamp.getSeconds()}`;
    }

    getUniqueIdentifier(tag){
        tag = tag || '';
        switch(tag){
            case 'hash':
                return this.generateHash();
            case 'timestamp':
                return this.generateTimestamp();
            default:
                return '';
        }
    }

    postfix({symbol, separator, outFile, postfix}){
        separator = postfix ? separator || '' : '';
        const parts =  outFile.split('.');
        let newOutFile = '';
        for(let i = 0; i < parts.length; i++){
            if(i !== parts.length - 1) newOutFile += parts[i];
            else newOutFile += separator + postfix + '.' + parts[i];
        }
        return `${symbol}${newOutFile}`;
    }

    prefix({symbol, separator, outFile, prefix}){
        separator = prefix ? separator || '' : '';
        return `${symbol}${prefix}${separator}${outFile}`;
    }

    rename(inFile = this.inFile, outFile = this.outFile, separator = this.separator, affix = this.affix, concatenate = this.concatenate){
        if(!inFile){
            console.error('No input file specified')
            return
        }

        const symbol = os.type() === 'Windows_NT'? "\\" : "/";
        let filename =  outFile ? path.resolve(path.dirname(outFile)) : path.resolve(path.dirname(inFile));
        // TODO: handle directory changes using -o tag
        outFile = outFile ? path.basename(outFile) : path.basename(inFile);
        concatenate = this.getUniqueIdentifier(concatenate);
       

        switch(affix){
            case 'postfix':
                    filename += this.postfix({symbol, separator, outFile, postfix: concatenate}) ; 
                break;
            case 'prefix':
            default:
                filename += this.prefix({symbol, separator, outFile, prefix: concatenate}); 
        }
        fs.rename(path.resolve(inFile), filename, (err) => {
            if(err) console.error(err);
        });
    }
}