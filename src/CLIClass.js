const Nomenclature = require('./NomenclatureClass');
const path = require('path');
const commander = require('commander');
const fs = require('fs');

module.exports = class CLI {
    constructor(input = process.argv){
        const packageJSONFile = fs.readFileSync(path.resolve('./package.json'));
        const packageJSON = JSON.parse(packageJSONFile);
        commander
        .version(packageJSON.version, '-v, --version')
        .usage('<file> [options]')
        .option('-i, --in-file <inFile>', 'Input file to be renamed')
        .option('-o, --out-file [outFile]', 'Output file name')
        .option('--prefix', 'Prefix the filename')
        .option('--postfix', 'Postfix the filename')
        .option('--dash', 'Dash separate the prefix or postfix')
        .option('--dot', 'Dot separate the prefix or postfix')
        .option('--underscore', 'Underscore saparate the prefix or postfix')
        .option('-h, --hash', 'Append a hash (prefix by default)')
        .option('-t, --timestamp', 'Append a timestamp to the filename (prefix by default)')
        .parse(input);

        if(commander.inFile) this.inFile = path.resolve(commander.inFile);
        else {
            console.error('No input file specified');
            return;
        } 
        if(commander.outFile) this.outFile = path.resolve(commander.outFile);
        if(commander.prefix || commander.postfix) this.affix = commander.prefix ? 'prefix' : commander.postfix ? 'postfix' : '';
        if(commander.dash || commander.dot || commander.underscore) this.separator = commander.dash ? '-' : commander.dot ? '.' : commander.underscore ? '_' : '';
        if(commander.hash || commander.timestamp) this.concatenate = commander.hash ? 'hash' : commander.timestamp ? 'timestamp' : '';
        const nom = new Nomenclature(
            {
                inFile: this.inFile,
                outFile: this.outFile,
                separator: this.separator, 
                affix: this.affix,
                concatenate: this.concatenate
            }
        )
        nom.rename();
    }
}